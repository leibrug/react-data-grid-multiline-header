'use strict';

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

var DEFAULTS = {
  minWidth: 120,
  maxWidth: 300,
  hPaddingCell: 16,
  hPaddingSort: 22,
  vPaddingCell: 16,
  lineHeight: 16 * 1.5,
  font: '700 16px -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif'
};
var H_PADDING_CORRECTION = 4;

var ReactDataGridMultilineHeader = function () {
  function ReactDataGridMultilineHeader(columns) {
    var _this = this;

    var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    classCallCheck(this, ReactDataGridMultilineHeader);

    this._getLineWidth = function (line) {
      return Math.ceil(_this._ctx.measureText(line).width);
    };

    this._getColumnHorizontalPadding = function (column) {
      return _this._config.hPaddingCell + (column.sortable ? _this._config.hPaddingSort : 0) + H_PADDING_CORRECTION;
    };

    this._updateLines = function (text, columnContentWidth) {
      var lines = [text];
      var currentLine = 0;
      do {
        if (_this._getLineWidth(lines[currentLine]) > columnContentWidth) {
          if (lines.length === currentLine + 1) {
            lines.push('');
          }
          lines[currentLine + 1] = (lines[currentLine].substring(lines[currentLine].lastIndexOf(' ') + 1) + ' ' + lines[currentLine + 1]).trim();
          lines[currentLine] = lines[currentLine].substring(0, lines[currentLine].lastIndexOf(' '));
        } else {
          currentLine += 1;
        }
      } while (lines.some(function (line) {
        return _this._getLineWidth(line) > columnContentWidth;
      }));

      _this._lines = Math.max(_this._lines, lines.length);
    };

    this._setColumnWidth = function (column) {
      var padding = _this._getColumnHorizontalPadding(column);
      var maxTextWidth = (column.width || _this._config.maxWidth) - padding;
      var textWidth = _this._getLineWidth(column.name);

      if (textWidth <= maxTextWidth) {
        column.width = Math.max(_this._config.minWidth, textWidth + padding);
      } else {
        var longestWordWidth = column.name.split(' ').reduce(function (previousValue, currentValue) {
          return Math.max(previousValue, _this._getLineWidth(currentValue));
        }, 0);
        var columnContentWidth = Math.max(maxTextWidth, longestWordWidth);
        column.width = columnContentWidth + padding;
        _this._updateLines(column.name, columnContentWidth);
      }

      return column;
    };

    this._columns = columns;
    this._config = Object.assign(DEFAULTS, config);
    this._lines = 1;
    this._ctx = document.createElement('canvas').getContext('2d');
    this._ctx.font = this._config.font;
  }

  createClass(ReactDataGridMultilineHeader, [{
    key: 'headerRowHeight',
    get: function get$$1() {
      return this._lines * this._config.lineHeight + this._config.vPaddingCell;
    }
  }, {
    key: 'columns',
    get: function get$$1() {
      return this._columns.map(this._setColumnWidth);
    }
  }]);
  return ReactDataGridMultilineHeader;
}();

module.exports = ReactDataGridMultilineHeader;
