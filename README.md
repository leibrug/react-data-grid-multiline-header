## About

This is a helper class for [react-data-grid](https://www.npmjs.com/package/react-data-grid) component that enables multiline text in grid header cells.

For usage and example code, check http://react-data-grid-multiline-header.leibrug.pl
